<!DOCTYPE html>
<html>
	<head>
		<title>Сайт</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="style/indexs.css">
		<script type="text/javascript" src="jquery.min.js"></script>
	</head>
	<body>
		<div class = 'login'>
			<p>Введите логин</p>
			<input type="text" id = 'login'>
		</div>
		<div class="msgBox">
			<div class = 'top'></div>
			<div class = 'chat'>
				<iframe src="chat.php" id = "iframe"></iframe>
			</div>	
			<div class = 'bottom'>
				<div class = 'text-and-button'>
					<input type="text" id = 'message' value="Отправить">
					<button id = 'send'><img src="image/button2.jpg"></button>
				</div>
			</div>	
		</div>
	</body>
	<script type="text/javascript" src="script/index.js"></script>
</html>