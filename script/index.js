$('#send').click(function(){
	var login =   $('#login').val(); 
	var message = $('#message').val(); 
    var date = new Date();
    var time = date.getHours() + ":" + date.getMinutes();
    var data = {'login':login,'message':message,'time':time};
    console.log(JSON.stringify(data)); 

	if(!checkLogin(login)) return;
    if(!checkMessage(message)) return;
    $.ajax({
        type:'POST',
        url:'save.php',
        data:"param="+JSON.stringify(data),
    }).done(function( result ){
        $('#iframe').each(function() {
            this.contentWindow.location.reload(true);
        });
    });
});

function checkLogin(login){
    if(/^[a-zA-Z1-9-а-яА-Я]+$/.test(login) === false){alert('В логине можно использовать буквы и цифры!'); return false;}
    if(login.length < 4 || login.length > 20){ alert('В логине должен быть от 4 до 20 символов!'); return false;}
    if(parseInt(login.substr(0, 1))){alert('Логине должен начинаться с буквы!'); return false;}
    return true;
}

function checkMessage(message){
    if(message.length < 1){ alert('Нельзя отправлять пустое сообщение!'); return false;}
    if(message.length > 1024){ alert('Максимальная длина сообщения 1024 символа!'); return false;}
    return true;
}

$('#message').click(function(){
    $(this).val("");
});