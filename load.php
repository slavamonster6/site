<?php
$link = mysqli_connect("localhost", "root", "","site");
if ($link == false)
{
	//print("Ошибка: Невозможно подключиться к MySQL " . mysqli_connect_error());
}
else 
{
	//print("Соединение установлено успешно");
}

$sql = 'SELECT * FROM `message`';
$result = mysqli_query($link, $sql);
if ($result == false) 
{
	//print("Произошла ошибка при выполнении запроса");
}

$data = array();
$rows = mysqli_num_rows($result);

for ($i = 0 ; $i < $rows ; ++$i)
{
	$row = mysqli_fetch_array($result);
	$array = array('id' => $row['id'],'login' => $row['login'],'message' => $row['message'],'time' => $row['time']);
	$data[$i] = $array;
}

echo json_encode($data);